set nocompatible              " be improved, required for Vundle
filetype off                  " required for Vundle

call plug#begin('~/.vim/plugged')

" User defined plugins
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-syntastic/syntastic'
"Plug 'tpope/vim-sleuth'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tpope/vim-fugitive'  " git wrapper
"Plug 'ervandew/supertab'
Plug 'airblade/vim-gitgutter'
Plug 'vim-airline/vim-airline'
Plug 'easymotion/vim-easymotion'
Plug 'terryma/vim-multiple-cursors'
Plug 'jiangmiao/auto-pairs'


"Plug 'valloric/youcompleteme'

"colorScheme
Plug 'morhetz/gruvbox'

" Initialize plugin system
call plug#end()


let mapleader = ","

"syntastic conf
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_quiet_messages = { "level" : "warnings"  }
let g:syntastic_loc_list_height=2
"//


"nerdcommenter conf"
let g:NERDDefaultAlign = 'start'
let g:NERDRemoveExtraSpaces = 0
nmap <C-_> :call NERDComment('n','toggle')<CR>
vmap <C-_> :call NERDComment('x','toggle')<CR>
"

" "NERDTree
" "open nerdtree C-k C-b
" "map ,, :NERDTreeToggle<cr>
map <leader>b :NERDTreeToggle<cr>
"make ctrp working from nerdtree
"let g:NERDTreeChDirMode       = 2
"let g:ctrlp_working_path_mode = 'rw'
"""

set lazyredraw

set shiftwidth=2
set softtabstop=2
set expandtab
set tabstop=2
set autoindent
set smartindent


"show whitespaces
set listchars=eol:¬,tab:__,extends:>,precedes:<,space:.
set list


autocmd Filetype python setlocal ts=2 sw=2 sts=2 noexpandtab
autocmd Filetype javascript setlocal ts=2 sw=2 sts=2 expandtab
autocmd Filetype html setlocal ts=2 sw=2 sts=2 expandtab


if has('mouse')
  set mouse=a
endif

" " Configure backspace so it acts as it should act
" set backspace=eol,start,indent

" "turn off backups
set nobackup
set nowb
set noswapfile

" " ================ Persistent Undo ==================
" " Keep undo history across sessions, by storing in file.
" " Only works all the time.
if has('persistent_undo')
  silent !mkdir ~/.vim/backups > /dev/null 2>&1
  set undodir=~/.vim/backups
  set undofile
endif

" "set colorscheme
colorscheme gruvbox
set background=dark


syntax on
set number


" "Search
set ic
set hls
set is

" "bash like extention for command line
set wildmode=longest:list,full

" "no equal windows
set noequalalways
set winheight=20
set fileencoding=utf-8



" "Open new windows in right
set splitright



" "let g:move_key_modifier = 'C'

" "airline
set laststatus=2

"open file from line where it was " .viminfo must be accesable
if has("autocmd")
	au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
		\| exe "normal! g'\"" | endif
endif

" Map ctrl-movement keys to window switching
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>


"map <silent> <C-j> :call WinMove('j')<CR>
"map <silent> <C-k> :call WinMove('k')<CR>
"map <silent> <C-h> :call WinMove('h')<CR>
"map <silent> <C-l> :call WinMove('l')<CR>

"function! WinMove(key)
	"let t:curwin = winnr()
	"exec "wincmd ".a:key
	"if (t:curwin == winnr())
		"if (match(a:key,'[jk]'))
			"wincmd v
		"else
			"wincmd s
		"endif
		"exec "wincmd ".a:key
	"endif
"endfunction

"filetype specific
" autocmd BufRead,BufNewFile   *.c,*.h,*.java,*.py setlocal ts=2 sw=2

" Automatically removing all trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e


" Toggle paste mode
nmap <silent> <F4> :set invpaste<CR>:set paste?<CR>
imap <silent> <F4> <ESC>:set invpaste<CR>:set paste?<CR>

" Allows you to enter sudo pass and save the file
" " when you forgot to open your file with sudo
cmap w!! %!sudo tee > /dev/null %"

" Allow to copy/paste between VIM instances
" "copy the current visual selection to ~/.vbuf
vmap <Leader>y :w! ~/.vbuf<CR>
" "copy the current line to the buffer file if no visual selection
nmap <Leader>y :.w! ~/.vbuf<CR>
" "paste the contents of the buffer file
nmap <Leader>p :r ~/.vbuf<CR>"

"copy
nmap <C-c> "+y
vmap <C-c> "+y
nmap <C-v> "+p
vmap <C-v> "+p
set autowrite
set hidden
nmap <C-p> :CtrlP .<CR>
vmap <C-p> :CtrlP .<CR>
